/**************************
*  myforest deer stalker  *
*  PhoneGap app           *
**************************/

// test codes - S74LK, 5TALK

var app = {

	// data storage
	storage: window.localStorage,
	properties: {},
	currentPropertyId: null,
	currentProperty: null,

	// flags
	tabsShowing: false,

	// data endpoints
	endpoints: {
		getProperty: "https://staging.sylva.org.uk/myforest/stalkerapp/get-property",
		sync: "https://staging.sylva.org.uk/myforest/stalkerapp/sync"
		// getProperty: "http://sylva.local/myforest/stalkerapp/get-property",
		// sync: "http://sylva.local/myforest/stalkerapp/sync"
		// getProperty: "fakeNewsLoginResponse.json"
		// sync: "fakeNewsSubmitResponse.json"
	},

	// dom refs
	dom: {
		$archive_records: $("#archive_records"),
		$btn_login: $("#btn_login"),
		$btn_add_record: $("#btn_add_record"),
		$btn_confirm_submission: $("#btn_confirm_submission"),
		$btn_new_record: $("#btn_new_record"),
		$btn_submit_records: $("#btn_submit_records"),
		$home_contents: $("#home_contents"),
		$input_loginCode: $("#input_loginCode"),
		$new_record_species: $("#new_record_species"),
		$new_record_sex: $("#new_record_sex"),
		$new_record_count: $("#new_record_count"),
		$properties: $("#properties"),
		$property_name: $(".property_name"),
		$records: $("#records"),
		$sync_icon: $("#sync_icon"),
		$submission_records: $("#submission_records"),
		$submission_date: $("#submission_date"),
		$submission_property: $("#submission_property"),

		new_record_form: document.getElementById("new_record_form"),
		tabs: document.getElementById("tabs")
	},

	// application constructor
	initialize: function () {
		this.bindEvents();
		this.initUI();
	},

	// load serialised property data from local store
	initProperties: function () {
		// load all stored properties
		var propertyData = this.storage.getItem("properties");
		if (!!propertyData) {
			// set local value
			this.properties = JSON.parse(propertyData);
			// populate property list
			this.populateProperties();
			// load selected property id
			this.currentPropertyId = this.storage.getItem("currentPropertyId");
			// select current property
			if (this.currentPropertyId) this.selectProperty(this.currentPropertyId);

		} else {
			// hide tabs
			this.dom.tabs.style.visibility = "hidden";
		}
	},

	// populate property list
	populateProperties: function () {
		// clear list
		app.dom.$properties.empty();
		// each property
		$.each(this.properties, function (propertyId, property) {
			// create property list item
			this.listing = $("<li>").append(
				$("<a href='#item'>").addClass("pull-right icon icon-close"),
				$("<span>").addClass("padded-list").text(property.property_name)
			).click(function () {
				app.selectProperty(propertyId);
			}).appendTo(app.dom.$properties);
		});
	},

	// populate records list
	populateRecords: function () {
		// clear list
		app.dom.$records.empty();
		app.dom.$submission_records.empty();
		
		// each record
		$.each(this.currentProperty.records, function () {
			// create record name string
			var recordString = this.species+" "+((this.sex)=="m" ? "male" : "female")+" &times; "+this.count;
			// create record list item
			var recordListing = $("<li>").append(
				$("<a class='btn_delete_record' href='#item'>").addClass("pull-right icon icon-close"),
				$("<span>").addClass("padded-list").html(recordString));
			// add main record listing
			recordListing.appendTo(app.dom.$records);
			// add submission listing
			var submissionListing = recordListing.clone();
			submissionListing.children(".btn_delete_record").remove();
			submissionListing.appendTo(app.dom.$submission_records);
		});

		// disable submit button if no unsubmitted records
		this.dom.$btn_submit_records.prop("disabled", (this.currentProperty.records.length == 0));

		// populate submitted records list
		this.populateArchive();
	},

	// populate submitted records list
	populateArchive: function () {
		// clear list
		app.dom.$archive_records.empty();
		// each submission
		$.each(this.currentProperty.submissions, function () {
			// create date header
			$("<li>").addClass("separator").appendTo(app.dom.$archive_records);
			var submissionTitle = $("<li>").addClass("divider").text(this.submission_date).appendTo(app.dom.$archive_records);
			if (this.imported == "1") submissionTitle.prepend("<div class='pull-right icon icon-check'></div>");
			// each record for this date
			$.each(this.records, function () {
				// create record name string
				var recordString = this.species+" "+((this.sex)=="m" ? "male" : "female")+" &times; "+this.count;
				// create record list item
				var recordListing = $("<li>").addClass("thin").append($("<span>").addClass("padded-list").html(recordString))
					.appendTo(app.dom.$archive_records);
			});
		});
	},

	// bind any events that are required on startup.
	// common events are: 'load', 'deviceready', 'offline', and 'online'
	bindEvents: function () {
		document.addEventListener("deviceready", this.onDeviceReady, false);
	},

	// deviceready event handler
	onDeviceReady: function () {
		// get sync status from local store and hide/show icon
		app.synced = app.storage.getItem("synced");
		app.updateSyncDisplay();
		// get property data from local store
		app.initProperties();
		// check if app needs to sync
		app.checkSync();
		// add listener to sync when device comes online
		document.addEventListener("online", app.checkSync, false);
		// show properties tab
		phonon.tab().setCurrentTab("home", 1);
	},

	// bind ui taps etc.
	initUI: function () {
		// setup phonon framework
		phonon.options({
			navigator: {
				defaultPage: "home",
				hashPrefix: "!", // default #!pagename
				animatePages: true,
				enableBrowserBackButton: true,
				templateRootDirectory: "",
				//defaultTemplateExtension: 'html', // if you use page templates
				useHash: true
			},
			i18n: null
		});
		// start phonon framework
		phonon.navigator().start();

		// login with code button
		this.dom.$btn_login.click(function () { app.getProperty(); });
		// open panel with new record form
		this.dom.$btn_new_record.click(function () { phonon.panel("#addRecord").open(); });
		// open panel with submit records form
		this.dom.$btn_submit_records.click(function () { app.submissionPanel(); });
		// submit new record form
		this.dom.$btn_add_record.click(function () {
			// add the record
			app.addRecord();
			// reset the form
			app.dom.new_record_form.reset();
			// close panel
			phonon.panel("#addRecord").close();
		});
		// confirm submit records
		this.dom.$btn_confirm_submission.click(function () { app.submitRecords(); });
		// sync button info
		this.dom.$sync_icon.click(function () {
			phonon.alert("<p>Your data is not synchronised with the server.</p>"+
				"<p>Synchronising will happen automatically when you have an internet connection.</p>", "sync", false, "ok");
		});
	},

	// setup the submission panel
	submissionPanel: function () {
		this.dom.$submission_date.val(this.getDateString("now", true));
		// document.getElementById("submission_date").valueAsDate = new Date();
		this.dom.$submission_property.html(this.currentProperty.property_name);
		phonon.panel("#submitRecords").open();
	},

	// submit records
	submitRecords: function () {
		// get datestring
		var dateString = this.getDateString(this.dom.$submission_date.val(), true);
		// create a submission
		var newSubmission = {
			appId: this.getIdString(),
			submission_date: dateString,
			records: this.currentProperty.records
		};
		// add to refs
		this.currentProperty.submissions.push(newSubmission);
		// clear live records array
		this.currentProperty.records = [];
		// save data to local storage
		app.saveProperties();
		// sync data
		this.sync();

		// clear dom
		app.dom.$records.empty();
		app.dom.$submission_records.empty();
		// repopulate archive
		app.populateArchive();
		// close submission panel
		phonon.panel("#submitRecords").close();
		// show success alert
		phonon.alert("submission successful", "submission", false, "ok");
	},

	// check if app needs to sync
	checkSync: function () {
		if (!app.synced) app.sync();
	},

	// show / hide sync icon
	updateSyncDisplay: function () {
		// hide icon if nothing is loaded yet
		if ($.isEmptyObject(this.properties)) this.dom.$sync_icon.hide();
		// else hide/show based on actual sync status
		else {
			if (this.synced) this.dom.$sync_icon.hide();
			else this.dom.$sync_icon.show();
		}
	},

	// sync data with server
	sync: function () {
		// check for internet
		if (this.hasConnection()) {

			// make ajax post
			$.post(this.endpoints.sync, {
				properties: app.getPropertyData()
			}, function (res) {
				if (res.result == "success") {
					// set ids for new records
					app.setNewIds(res.changedData);
					// set synced flags
					app.storage.setItem("synced", true);
					app.synced = true;
					// remove sync icon
					app.updateSyncDisplay();
					// update archve display
					app.populateArchive();
				}
			}, "json");

		} else {
			// set synced flags
			app.storage.setItem("synced", false);
			app.synced = false;
			// add sync icon
			app.updateSyncDisplay();
		}
	},

	// get clean property data (no dom refs etc)
	getPropertyData: function () {
		// init data
		var propertyData = [];
		// each property
		$.each(this.properties, function () {
			propertyData.push({
				id: this.id,
				records: this.records,
				submissions: this.submissions
			});
		});
		return propertyData;
	},

	// give database ids to records and submissions that only have local ids
	setNewIds: function (properties) {
		
		// each passed property array
		$.each(properties, function () {
			// each new record id
			$.each(this.recordIds, function (appId, newId) {
				// all properties
				$.each(app.properties, function () {
					// live records 
					$.each(this.records, function () {
						app.updateAppId(this, appId, newId);
					});
					// records already in submissions
					$.each(this.submissions, function () {
						$.each(this.records, function () {
							app.updateAppId(this, appId, newId);
						});
					});
				});
			});
			// each new submission id
			$.each(this.submissionIds, function (appId, newId) {
				// update all submissions in all properties
				$.each(app.properties, function () {
					$.each(this.submissions, function () {
						app.updateAppId(this, appId, newId);
					});
				});
			});
			// each imported submission
			$.each(this.submissionsImported, function (subId, imported) {
				// each local property
				$.each(app.properties, function () {
					// each submission for this property
					$.each(this.submissions, function () {
						// update imported value
						if (this.id == subId) this.imported = imported;
					});
				});
			});
		});
	},

	// find an entry with an app id and replace with real db id
	updateAppId: function (element, appId, newId) {
		// if appIds match
		if (element.appId == appId) {
			// set proper database id
			element.id = newId;
			// remove temporary id
			delete element.appId;
		}
	},

	// setup property screen with user data
	initProperty: function () {
		// show tabs
		if (!this.tabsShowing) {
			this.dom.tabs.style.visibility = "visible";
			this.dom.$home_contents.attr("data-disable-swipe", false);
			phonon.tab().init("home");
			this.tabsShowing = true;
		}
		// populate record list
		this.populateRecords();
	},

	// send login code to backend and get property data
	getProperty: function () {
		// check for internet
		if (this.hasConnection()) {

			// get code from input
			var enteredCode = this.dom.$input_loginCode.val();
			// disable login button
			this.dom.$btn_login.text("adding property...").prop("disabled", true);

			// send code to backend
			$.get(this.endpoints.getProperty, {loginCode: enteredCode}, function (res) {
				// re-enable login button
				app.dom.$btn_login.html("<i class='icon icon-add'></i>add property").prop("disabled", false);
				// if success
				if (res.result == "success") {
					// clear input
					app.dom.$input_loginCode.val("");
					// add property data to local storage
					app.addProperty(res.property);
					// show alert
					var added = phonon.alert("property added successfuly", "success", false, "ok");
					// select property when alert is confirmed
					added.on("confirm", function() {
						app.selectProperty(res.property.id);
					} );
				}
				// show error if code not found
				else {
					phonon.alert("please check your code and try again", "code not found", false, "ok");
				}
			}, "json");
		}
		// show error if no connection
		else {
			phonon.alert("you need an internet connection to add a property", "no internet", false, "ok");
		}
	},

	// add a new record from inputs
	addRecord: function () {
		// create record object from inputs
		var newRecord = {
			id: 0,
			appId: this.getIdString(),
			property_id: this.currentProperty.id,
			species: this.dom.$new_record_species.val(),
			sex: $("input[name=new_record_sex]:checked").val(),
			count: this.dom.$new_record_count.val()
		};
		// add to local objects
		this.currentProperty.records.push(newRecord);
		// update dom
		this.populateRecords();
		// save to storage
		this.saveProperties();
		// sync data
		this.sync();
	},

	// generate random character string
	getIdString: function () {
		return Math.random().toString(36).substring(2)
	},

	// add a property from db data
	addProperty: function (propertyData) {
		// add property data to local array
		this.properties[String(propertyData.id)] = propertyData;
		// save to local storage
		this.saveProperties();
		// populate list
		this.populateProperties();
	},

	// select a property
	selectProperty: function (propertyId) {
		// unhighlight old listing
		if (app.currentProperty) app.currentProperty.listing.removeClass("selected");

		// set local variables
		app.currentPropertyId = propertyId;
		app.currentProperty = app.properties[propertyId];
		// save to local storage
		app.storage.setItem("currentPropertyId", propertyId);

		// highlight listing
		app.currentProperty.listing.addClass("selected");

		// show name in titles
		app.dom.$property_name.text(app.currentProperty.property_name);

		// show records tab
		app.initProperty();
		phonon.tab().setCurrentTab("home", 2);
	},
	// deselect all properties
	/*deselectProperty: function (propertyId) {
		// clear local variables
		app.currentPropertyId = null;
		app.currentProperty = null;
		// clear local storage
		app.storage.removeItem("currentPropertyId");
		app.storage.removeItem("currentProperty");
		// select property nav
		phonon.tab().setCurrentTab("home", 1);
	},*/

	// save property data to local storage
	saveProperties: function () {
		// parse into string
		var propertiesString = JSON.stringify(app.properties);
		// set stored json string
		app.storage.setItem("properties", propertiesString);
	},

	// get formatted date string
	getDateString: function (date, forDatePicker) {
		var now = (date == "now") ? new Date() : new Date(date);
		var dd = now.getDate();
		var mm = now.getMonth() + 1; // January is 0, so always add + 1

		var yyyy = now.getFullYear();
		if (dd < 10) { dd = "0" + dd };
		if (mm < 10) { mm = "0" + mm };

		if (forDatePicker) return yyyy+"-"+mm+"-"+dd;
		else return mm+"/"+dd+"/"+yyyy;
	},

	// return true if we have an internet connection
	hasConnection: function () {
		var networkState = navigator.connection.type;
		return (networkState !== Connection.NONE);
	}
};
